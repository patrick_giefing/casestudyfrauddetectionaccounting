# CaseStudy FraudDetectionAccounting #

Reading journal entries from CSV/Excel into Postgres, then showing query results in Grafana.

![Talend process](./src/main/docs/screenshot/talend.png "Talend process")

![Grafana Dashboard](./src/main/docs/screenshot/grafana-dashboard.png "Grafana Dashboard")