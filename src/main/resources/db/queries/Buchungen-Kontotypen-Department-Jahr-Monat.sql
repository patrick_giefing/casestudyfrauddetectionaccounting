select distinct account_type, m.id, m.name,
(select sum(je.amount_functional) from journal_entry je, users u, account a1
    where je.id_preparer = u.user_name and je.id_account = a.id_account and
          u.department = 'IT' and a1.account_type = a.account_type and je.fiscal_year = 2013 and
          je.fiscal_period = m.id and je.amount_functional < 0) as "Soll",
(select sum(je.amount_functional) from journal_entry je, users u, account a1
    where je.id_preparer = u.user_name and je.id_account = a.id_account and
          u.department = 'IT' and a1.account_type = a.account_type and je.fiscal_year = 2013 and
          je.fiscal_period = m.id and je.amount_functional > 0) as "Haben"
from account a, monat m
order by a.account_type, m.id