WITH
benford(first_digit, probability_real, probability_theoretical) AS
(
SELECT f.first_digit, f.probability_real, CAST(log(1. + 1./CAST(first_digit AS INT)) AS NUMERIC(10,8)) probability_theoretical, f.id_account, f.account_entry_count
FROM (
	SELECT first_digit, CAST(COUNT(1) AS NUMERIC(8,2)) /(SELECT count(*) FROM journal_entry je where je.id_account = t.id_account) probability_real, t.id_account, (SELECT count(*) FROM journal_entry je where je.id_account = t.id_account) account_entry_count FROM
	(
		SELECT SUBSTRING(CAST(a AS VARCHAR(100)),1,1) first_digit, id_account
		FROM (SELECT je.id_account, abs(je.amount_functional) a FROM journal_entry je) b1
		WHERE SUBSTRING(CAST(a AS VARCHAR(100)),1,1) <> '0'
		LIMIT (SELECT count(*) FROM journal_entry)
	) t
	GROUP BY id_account, first_digit
) f
where account_entry_count >= 200
ORDER BY id_account asc, first_digit ASC
)
select * from (
    SELECT a.account_name, avg(LEAST(b.probability_real, b.probability_theoretical) / GREATEST(b.probability_real, b.probability_theoretical)) as "Benford Coefficient" FROM benford b, account a where b.id_account = a.id_account group by a.account_name
) c where "Benford Coefficient" < .7 order by "Benford Coefficient"