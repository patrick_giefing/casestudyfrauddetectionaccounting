select 'Buchungssaldo gesamt' as "Kategorie", sum(amount_functional) as "Saldo" from journal_entry
union
select 'Buchungssaldo pro Jahr - Betrag aufsummiert', sum(s) as "Saldo" from (select sum(amount_functional) s from journal_entry group by extract(year from date_entry)) a
union
select 'Buchungssaldo pro Buchung - Betrag aufsummiert', sum(s) as "Saldo" from (select abs(sum(amount_functional)) s from journal_entry group by id_journal_entry) a