select * from (
  select id_journal_entry, abs(ROUND(sum(amount_functional))) as diff from journal_entry je group by id_journal_entry
) a where a.diff <> 0;