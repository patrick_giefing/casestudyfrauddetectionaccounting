select
extract(year from date_entry) as "Jahr",
h.*, count(*) "Anzahl Buchungen"
from journal_entry je, holiday h
where je.date_entry = h.date
group by "Jahr", h.date, h.title
order by "Jahr", "Anzahl Buchungen" desc