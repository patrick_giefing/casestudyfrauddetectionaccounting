select
     CASE
  WHEN (day_of_week = 1) THEN 'Montag'
  WHEN (day_of_week = 2) THEN 'Dienstag'
  WHEN (day_of_week = 3) THEN 'Mittwoch'
  WHEN (day_of_week = 4) THEN 'Donnerstag'
  WHEN (day_of_week = 5) THEN 'Freitag'
  WHEN (day_of_week = 6) THEN 'Samstag'
  WHEN (day_of_week = 7) THEN 'Sonntag'
 END AS metric
     , count(*) value from (
                  select extract(isodow from date_entry) day_of_week
                  from journal_entry
              ) a group by day_of_week order by day_of_week;

select
       year,
     CASE
  WHEN (day_of_week = 1) THEN 'Montag'
  WHEN (day_of_week = 2) THEN 'Dienstag'
  WHEN (day_of_week = 3) THEN 'Mittwoch'
  WHEN (day_of_week = 4) THEN 'Donnerstag'
  WHEN (day_of_week = 5) THEN 'Freitag'
  WHEN (day_of_week = 6) THEN 'Samstag'
  WHEN (day_of_week = 7) THEN 'Sonntag'
 END AS metric
     , count(*) "value" from (
                  select extract(isodow from date_entry) day_of_week,
                  extract(year from date_entry) "year"
                  from journal_entry
              ) a where year in (2013, 2014) group by year, day_of_week order by day_of_week;