select * from (
select a.account_name as "Konto",
(select abs(sum(amount_functional)) from journal_entry je where je.id_account = a.id_account and je.fiscal_year = 2013 and je.amount_functional < 0) "Soll 2013",
(select sum(amount_functional) from journal_entry je where je.id_account = a.id_account and je.fiscal_year = 2013 and je.amount_functional > 0) "Haben 2013",
(select abs(sum(amount_functional)) from journal_entry je where je.id_account = a.id_account and je.fiscal_year = 2014 and je.amount_functional < 0) "Soll 2014",
(select sum(amount_functional) from journal_entry je where je.id_account = a.id_account and je.fiscal_year = 2014 and je.amount_functional > 0) "Haben 2014"
from account a where account_type = 'Assets' order by a.account_name
) a where not "Soll 2013" is null or not "Haben 2013" is null or not "Soll 2014" is null or not "Haben 2014" is null