select j.id as "Jahr", m.id as "MonatNr", m.name as "Monat",
(select abs(sum(je.amount_functional)) from journal_entry je, account a where je.id_account = a.id_account and a.account_type = 'Assets' and je.fiscal_year = j.id and je.fiscal_period = m.id and je.amount_functional < 0) as "Soll",
(select sum(je.amount_functional) from journal_entry je, account a where je.id_account = a.id_account and a.account_type = 'Assets' and je.fiscal_year = j.id and je.fiscal_period = m.id and je.amount_functional > 0) as "Haben"
from jahr j, monat m order by j.id, m.id