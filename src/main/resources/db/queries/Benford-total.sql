-- https://rosettacode.org/wiki/Benford%27s_law#PL.2FpgSQL

WITH
constant(val) AS
(SELECT count(*) from journal_entry),
benford(first_digit, probability_real, probability_theoretical) AS
(
SELECT *,
	CAST(log(1. + 1./CAST(first_digit AS INT)) AS NUMERIC(10,8)) probability_theoretical
FROM (
	SELECT  first_digit, CAST(COUNT(1) AS NUMERIC(8,2)) /(SELECT val FROM constant) probability_real FROM
	(
		SELECT SUBSTRING(CAST(a AS VARCHAR(100)),1,1) first_digit
		FROM (SELECT abs(je.amount_functional) a FROM journal_entry je) b1
		WHERE SUBSTRING(CAST(a AS VARCHAR(100)),1,1) <> '0'
		LIMIT (SELECT val FROM constant)
	) t
	GROUP BY first_digit
) f
ORDER BY first_digit ASC
)
SELECT *
FROM benford CROSS JOIN
     (SELECT CAST(corr(probability_theoretical,probability_real) AS NUMERIC(10,8)) correlation
      FROM benford) c