select extract(year from date_entry) "year", u.department, u.full_name, count(*) "value"
from journal_entry je, users u where je.id_preparer = u.user_name and extract(year from date_entry) in (2013, 2014)
group by extract(year from date_entry), u.department, u.full_name;