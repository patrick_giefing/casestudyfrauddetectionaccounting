select * from (
                  select extract(year from date_entry) as "Jahr", 'Positiv' as "Richtung", sum(amount_functional) as "Summe"
                  from journal_entry je
                  where amount_functional >= 0
                  group by extract(year from date_entry)
                  union
                  select extract(year from date_entry) as "Jahr", 'Negativ' as "Richtung", sum(amount_functional) as "Summe"
                  from journal_entry je
                  where amount_functional < 0
                  group by extract(year from date_entry)
              ) a order by "Jahr", "Richtung"