select * from (
                         select a.*,
                                a.functional_closing_balance - a.functional_opening_balance -
                                a.account_year_sum diff_from_account_year_sum
                         from (
                                  select b.*,
                                         (select COALESCE(sum(je.amount_functional), 0)
                                          from journal_entry je
                                          where je.id_account = b.id_account
                                            and extract(year from date_effective) = b.fiscal_year) account_year_sum
                                  from balance b
                                  where b.fiscal_period = 13
                              ) a
                     ) c where diff_from_account_year_sum <> 0;
