select j.id, m.id as "MonatId", m.name as "Monat",
(select abs(sum(amount_functional)) from journal_entry je where je.id_account = a.id_account and je.fiscal_year = j.id and je.fiscal_period = m.id and amount_functional < 0) as "Soll",
(select sum(amount_functional) from journal_entry je where je.id_account = a.id_account and je.fiscal_year = j.id and je.fiscal_period = m.id and amount_functional > 0) as "Haben"
from account a, jahr j, monat m where a.account_name = 'Bank C - 5' order by j.id, m.id