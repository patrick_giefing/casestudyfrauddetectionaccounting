select je.fiscal_year as "Jahr", department as "Department", a.account_type as "Konto Typ", sum(je.amount_functional) as "Summe"
from journal_entry je, users u, account a
where je.id_preparer = u.user_name and je.id_account = a.id_account and je.fiscal_year in ($year) and department in ($departments)
group by fiscal_year, department, a.account_type