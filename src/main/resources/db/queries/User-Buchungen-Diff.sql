select a.*, abs("Buchungen 2013" - "Buchungen 2014") as "Buchungen Diff abs",
    round(cast(GREATEST("Buchungen 2013", "Buchungen 2014") as real) / cast(GREATEST(1, LEAST("Buchungen 2013", "Buchungen 2014")) as real)) as "Buchungen Diff rel" from (
    select u.user_name,
    (select count(*) from journal_entry je where je.id_preparer = u.user_name and je.fiscal_year = 2013) "Buchungen 2013",
    (select count(*) from journal_entry je where je.id_preparer = u.user_name and je.fiscal_year = 2014) "Buchungen 2014"
    from users u
) a where "Buchungen 2013" > 0 or "Buchungen 2014" > 0 order by "Buchungen Diff rel" desc, "Buchungen Diff abs" desc