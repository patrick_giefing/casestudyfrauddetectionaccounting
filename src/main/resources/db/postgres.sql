drop table if exists journal_entry;
drop table if exists balance;
drop table if exists account;
drop table if exists users;

create table journal_entry (
    id_journal_entry char(20) NOT NULL,
    line_number int NOT NULL,
    id_account varchar(10) NOT NULL, -- references...
    fiscal_year int NOT NULL,
    fiscal_period int NOT NULL,
    date_effective date NOT NULL,
    date_entry date NOT NULL,
    source_code char(2) NOT NULL,
    source_group varchar(30) NOT NULL,
    amount_functional numeric(20, 5) NOT NULL,
    currency_functional varchar(5) NOT NULL,
    id_preparer varchar(20) NOT NULL,
    preparer_department varchar(20) NOT NULL,
    reversal_indicator boolean NOT NULL,
    PRIMARY KEY(id_journal_entry, line_number)
);

create table users (
    user_name varchar(50) NOT NULL PRIMARY KEY,
    full_name varchar(100) NOT NULL,
    department varchar(100) NOT NULL,
    role varchar(100) NULL,
    title varchar(100) NULL
);

create table account (
    id_account varchar(10) NOT NULL PRIMARY KEY,
    account_name varchar(200) NOT NULL,
    account_type varchar(100) NOT NULL,
    account_sub_type varchar(100) NOT NULL,
    account_class varchar(100) NOT NULL,
    account_sub_class varchar(100) NOT NULL,
    ey_account_group_1 varchar(50) NULL,
    ey_account_group_2 varchar(50) NULL,
    ey_sub_ledger varchar(20) NULL,
    ey_account_bs_pl varchar(2) NULL,
    ey_management_account_ind boolean NULL
);

create table balance (
    id_account varchar(10) NOT NULL,
    fiscal_year int NOT NULL,
    fiscal_period int NOT NULL,
    functional_opening_balance numeric(20, 5) NOT NULL,
    functional_closing_balance numeric(20, 5) NOT NULL,
    functional_currency_code varchar(3) NOT NULL,
    PRIMARY KEY(id_account, fiscal_year, fiscal_period)
);

CREATE INDEX IDX_JE_DATE_ENTRY_YEAR ON journal_entry (extract(year from date_entry));
CREATE INDEX IDX_JE_DATE_ENTRY_DAY_OF_WEEK ON journal_entry (extract(isodow from date_entry));
CREATE INDEX IDX_JE_JOURNAL_ENTRY_PREPARER on journal_entry(id_preparer);
CREATE INDEX IDX_ACCOUNT on journal_entry(id_account);
CREATE INDEX IDX_BALANCE_FISCAL_YEAR on balance(fiscal_year);
CREATE INDEX IDX_BALANCE_FISCAL_PERIOD on balance(fiscal_period);
CREATE INDEX IDX_ACCOUNT_TYPE on account(account_type);
CREATE INDEX IDX_DEPARTMENT on users(department);
CREATE INDEX IDX_ID_ACCOUNT_ACCOUNT_TYPE on account(id_account, account_type);
CREATE INDEX IDX_PREPARER_ACCOUNT_YEAR_PERIOD_AMOUNT on journal_entry(id_preparer, id_account, fiscal_year, fiscal_period, amount_functional);

create table jahr (
    id smallint NOT NULL PRIMARY KEY
);

insert into jahr values (2013);
insert into jahr values (2014);

create table monat (
    id smallint NOT NULL PRIMARY KEY,
    name character varying(20) NOT NULL
);

insert into monat values (1, 'Jänner');
insert into monat values (2, 'Februar');
insert into monat values (3, 'März');
insert into monat values (4, 'April');
insert into monat values (5, 'Mai');
insert into monat values (6, 'Juni');
insert into monat values (7, 'Juli');
insert into monat values (8, 'August');
insert into monat values (9, 'September');
insert into monat values (10, 'Oktober');
insert into monat values (11, 'November');
insert into monat values (12, 'Dezember');

create table wochentag (
    id smallint NOT NULL PRIMARY KEY,
    name character varying(10)
);

insert into wochentag values (1, 'Montag');
insert into wochentag values (2, 'Dienstag');
insert into wochentag values (3, 'Mittwoch');
insert into wochentag values (4, 'Donnerstag');
insert into wochentag values (5, 'Freitag');
insert into wochentag values (6, 'Samstag');
insert into wochentag values (7, 'Sonntag');

create table holiday (
    date date NOT NULL PRIMARY KEY,
    title character varying(30) NOT NULL
);

insert into holiday values ('2013-01-01', 'Neujahr');
insert into holiday values ('2013-01-06', 'Heilige drei Könige');
insert into holiday values ('2013-03-31', 'Ostersonntag');
insert into holiday values ('2013-04-01', 'Ostermontag');
insert into holiday values ('2013-05-01', 'Staatsfeiertag');
insert into holiday values ('2013-05-09', 'Christi Himmelfahrt');
insert into holiday values ('2013-05-19', 'Pfingstsonntag');
insert into holiday values ('2013-05-20', 'Pfingstmontag');
insert into holiday values ('2013-05-30', 'Fronleichnam');
insert into holiday values ('2013-08-15', 'Mariä Himmelfahrt');
insert into holiday values ('2013-10-26', 'Nationalfeiertag');
insert into holiday values ('2013-11-01', 'Allerheiligen');
insert into holiday values ('2013-12-08', 'Mariä Empfängnis');
insert into holiday values ('2013-12-24', 'Heiliger Abend');
insert into holiday values ('2013-12-25', 'Christtag');
insert into holiday values ('2013-12-26', 'Stefanitag');
insert into holiday values ('2013-12-31', 'Silvester');

insert into holiday values ('2014-01-01', 'Neujahr');
insert into holiday values ('2014-01-06', 'Heilige drei Könige');
insert into holiday values ('2014-04-20', 'Ostersonntag');
insert into holiday values ('2014-04-21', 'Ostermontag');
insert into holiday values ('2014-05-01', 'Staatsfeiertag');
insert into holiday values ('2014-05-29', 'Christi Himmelfahrt');
insert into holiday values ('2014-06-08', 'Pfingstsonntag');
insert into holiday values ('2014-06-09', 'Pfingstmontag');
insert into holiday values ('2014-06-19', 'Fronleichnam');
insert into holiday values ('2014-08-15', 'Mariä Himmelfahrt');
insert into holiday values ('2014-10-26', 'Nationalfeiertag');
insert into holiday values ('2014-11-01', 'Allerheiligen');
insert into holiday values ('2014-12-08', 'Mariä Empfängnis');
insert into holiday values ('2014-12-24', 'Heiliger Abend');
insert into holiday values ('2014-12-25', 'Christtag');
insert into holiday values ('2014-12-26', 'Stefanitag');
insert into holiday values ('2014-12-31', 'Silvester');

----------------------------------
-- After data insert via Talend --
----------------------------------

drop table if exists journal_entry_id;

create table journal_entry_id as
select distinct cast(substr(id_journal_entry, 0, 5) as int) as year, cast(substr(id_journal_entry, 11) as bigint) id_journal_entry from journal_entry;

alter table journal_entry_id add PRIMARY KEY (year, id_journal_entry)

CREATE INDEX IDX_JOURNAL_ENTRY_ID_YEAR on journal_entry_id(year);

CREATE INDEX IDX_JOURNAL_ENTRY_ID_ID_JOURNAL_ENTRY on journal_entry_id(id_journal_entry);